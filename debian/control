Source: and
Section: misc
Priority: optional
Maintainer: Dario Minnucci <midget@debian.org>
Build-Depends: debhelper (>= 7.0.50~)
Rules-Requires-Root: no
Standards-Version: 3.8.3
Homepage: http://and.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/debian/and
Vcs-Git: https://salsa.debian.org/debian/and.git

Package: and
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Auto Nice Daemon
 The auto nice daemon activates itself in certain intervals and renices jobs
 according to their priority and CPU usage. Jobs owned by root are left alone.
 Jobs are never increased in their priority.
 .
 The renice intervals can be adjusted as well as the default nice level and
 the activation intervals. A priority database stores user/group/job tuples
 along with their renice values for three CPU usage time ranges. Negative nice
 levels are interpreted as signals to be sent to a process, triggered by CPU
 usage; this way, Netscapes going berserk can be killed automatically. The
 strategy for searching the priority database can be configured.
 .
 AND also provides network-wide configuration files with host-specific
 sections, as well as wildcard/regexp support for commands in the priority
 database.
